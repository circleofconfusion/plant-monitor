#include <Wire.h>
#include <Adafruit_AM2315.h>
#include <XBee.h>

XBee xbee = XBee();
XBeeAddress64 addr64 = XBeeAddress64(0, 0);

typedef union
{
  float number;
  uint8_t bytes[4];
} FLOATUNION_t;

Adafruit_AM2315 am2315;

int SOIL_PIN = A1;
int SOIL_PWR = 4;

void setup() {
  Serial.begin(9600);
  Serial.println("AM2315 Test!");
  Serial1.begin(9600);
  xbee.setSerial(Serial1);


  pinMode(SOIL_PWR, OUTPUT);
  digitalWrite(SOIL_PWR, LOW);

  if (! am2315.begin()) {
    Serial.println("Sensor not found, check wiring & pullups!");
    while (1);
  }
}

void loop() {
  FLOATUNION_t humidity;
  FLOATUNION_t temperature;
  am2315.readTemperatureAndHumidity(temperature.number, humidity.number);
  int soil = readSoil();
  
  uint8_t payload[10];
  
  memcpy(payload, humidity.bytes, sizeof(float));
  memcpy(&payload[4], temperature.bytes, sizeof(float));
  payload[8] = lowByte(soil);
  payload[9] = highByte(soil);
  
  
  ZBTxRequest zbTx = ZBTxRequest(addr64, payload, sizeof(payload));
  xbee.send(zbTx);

  delay(5000);
}

int readSoil()
{
  digitalWrite(SOIL_PWR, HIGH);
  delay(10);
  int value = analogRead(SOIL_PIN);
  digitalWrite(SOIL_PWR, LOW);
  return value;
}

