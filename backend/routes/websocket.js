const WebSocket = require('ws');

var wss;

module.exports = function(server) {
    wss = new WebSocket.Server({ server: server, clientTracking: true });

    wss.on('connection', function connection(ws, req) {
        ws.send('connected to plant monitor');
        ws.on('close', ws => { console.log("closed", ws)});
    });

}

module.exports.publish = function(data) {
    wss.clients.forEach(client => {client.send(data)});
}
