const dotenv = require('dotenv');
dotenv.load();

const mongoose = require('mongoose');
const db = mongoose.connection;

mongoose.connect(process.env.MONGODB_ADDRESS, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true
});
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => { console.log('connected')});
