const dotenv = require('dotenv');
dotenv.load();

const Sample = require('../models/sample');

const xbeeRx = require('xbee-rx');

const xbee = xbeeRx({
    serialport: process.env.XBEE_SERIAL_PORT,
    serialPortOptions: {
        baudrate: process.env.XBEE_BAUD_RATE
    },
    module: process.env.XBEE_MODULE,
    api_mode: +process.env.XBEE_API_MODE
});

const transmissionsRx = xbee
    .monitorTransmissions()
    .subscribe(
        rx => {
            const { humidity, temperature, soil } = splitValues(rx.data);
            saveSample('humidity', rx.remote64, humidity);
            saveSample('temperature', rx.remote64, temperature);
            saveSample('soil', rx.remote64, soil);
        }
    );

const dataRx = xbee
    .monitorIODataPackets()
    .subscribe(
        ioSamplePacket => {
            saveSample('soil', ioSamplePacket.remote64, ioSamplePacket.analogSamples.AD3);
        }
    );

async function saveSample(name, remote64, value) {
    const sample = new Sample({
        name,
        value,
        remote64
    });

    await sample.save();
}

function splitValues(buffer) {
    const humidity = buffer.readFloatLE(0);
    const temperature = buffer.readFloatLE(4);
    const soil = buffer.readUInt16LE(8);

    return {
        humidity,
        temperature,
        soil,
    };
}