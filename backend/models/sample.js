const {model, Schema} = require('mongoose');

const sampleSchema = new Schema({
    name: String,
    value: {
        type: Number,
        required: "Sensor 'value' required"
    },
    remote64: {
        type: String,
        required: "Sensor remote address required",
    },
    timestamp: {
        type: Date,
        default: Date.now
    }
});

module.exports = model('Sample', sampleSchema);
