const { model, Schema } = require('mongoose');

const deviceSchema = new Schema({
  name: {
    type: String,
    trim: true,
    required: "Device name is required",
    unique: true,
  },
  remote64: {
    type: String,
    required: "Address is required",
    unique: true,
  },
  description: {
    type: String,
  },
  sortOrder: {
    type: Number,
    default: 0,
  },
});

module.exports = model('Device', deviceSchema);