const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const caseSchema = new mongoose.Schema({
    name: {
        trim: true,
        type: String,
        required: "Case name is required"
    },
    soilMoisture: {
        type: Number,
        required: 'Soil moisture required'
    },
    temperature: {
        type: Number,
        required: 'Temperature required'
    },
    humidity: {
        type: Number,
        required: 'Humidity required'
    },
    timestamp: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model("Case", caseSchema);
