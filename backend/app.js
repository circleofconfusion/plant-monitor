const dotenv = require('dotenv');
const express = require('express');
const http = require('http');

// start up services
require('./services/xbee');
require('./services/db');

const app = express();
const server = http.createServer(app);

const websocket = require('./routes/websocket')(server);

dotenv.load();

app.use(express.static('static'));

server.listen(process.env.PORT, function listening() {
    console.log(`Listening on ${server.address().port}`);
});
